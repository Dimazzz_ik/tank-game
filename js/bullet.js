export function Bullet(data) {
    this.x = data.x || 0;
    this.y = data.y || 0;
    this.direction = data.direction;
    this.isFirstTick = true;
}
