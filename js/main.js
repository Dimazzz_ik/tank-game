import {Bullet} from './bullet.js';

let example = document.getElementById("example");

const TANK_SPEED = 2;
const BULLET_SPEED = 4;
const MAX_USER_BULLETS = 4;
const USER_TICK_SPRITE = 7;

let bulletCoolDown = 150; //таймоут пули
let isBulletCoolDown = false;

const direction = {
    right: 0,
    left: 1,
    down: 2,
    up: 3
};

let user;
let tanksSprite = null;
let enemyTanks = [];
let userBullets = [];

//control
let cLeft = 0;
let cDown = 0;
let cUp = 0;
let cRight = 0;
let shot = 0;

const map = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0],
    [0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0],
    [1, 1, 1, 1, 0, 0, 2, 2, 2, 0, 0, 1, 1, 1, 1],
    [2, 2, 1, 1, 0, 0, 2, 2, 2, 0, 0, 1, 1, 2, 2],
    [0, 0, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0],
    [0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0],
    [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0]
];

const boxSize = 32;
const bulletSize = 3;

const elements = {
    empty: {
        id: 0,
        fill: '#000'
    },
    brick: {
        id: 1,
        fill: '#FFA500'
    },
    block: {
        id: 2,
        fill: '#ffe6e3'
    },
    water: {
        id: 3,
        fill: '#2EC2FF'
    },
    tree: {
        id: 4,
        fill: '#008000'
    }
};

const userColor = '#FFF488';

example.width = map[0].length * boxSize;
example.height = map.length * boxSize;

const ctx = example.getContext('2d');

for (let i = 0; i < map.length; i++) {
    for (let j = 0; j < map[i].length; j++) {
        switch (map[i][j]) {
            case elements.empty.id: {
                ctx.fillStyle = elements.empty.fill;
                break;
            }
            case elements.brick.id: {
                ctx.fillStyle = elements.brick.fill;
                break;
            }
            case elements.block.id: {
                ctx.fillStyle = elements.block.fill;
                break;
            }
            case elements.water.id: {
                ctx.fillStyle = elements.water.fill;
                break;
            }
            case elements.tree.id: {
                ctx.fillStyle = elements.tree.fill;
                break;
            }
        }

        ctx.fillRect((j) * boxSize, (i) * boxSize, boxSize, boxSize);
    }
}

newUser();
setInterval(draw, 18);

function draw() {
    userMove();
    bulletsMove();
}

function newUser() {

    tanksSprite = new Image();
    tanksSprite.src = 'img/userTank.png';
    tanksSprite.onload = function () {
        user = {
            pos: [4 * boxSize, (map[0].length - 1) * boxSize],
            sprite: '',
            tick: 0,
            direction: 3
        };
        ctx.drawImage(tanksSprite, user.tick, user.direction * boxSize, boxSize, boxSize, user.pos[0], user.pos[1], boxSize, boxSize);
        ctx.fillStyle = '#fff';
        ctx.fillStyle = userColor;
    };

}

function userMoveTo(i, to) {
    ctx.fillStyle = "#000";
    ctx.fillRect(user.pos[0], user.pos[1], boxSize, boxSize);
    user.pos[i] += to;
    ctx.drawImage(tanksSprite, user.tick * boxSize, user.direction * boxSize, boxSize, boxSize, user.pos[0], user.pos[1], boxSize, boxSize);
    user.tick++;
    if (user.tick >= USER_TICK_SPRITE) {
        user.tick = 0;
    }
}

function userMove() {
    let x, y;
    if (cLeft) {
        y = Math.floor((user.pos[0] - TANK_SPEED) / boxSize);
        x = Math.floor((user.pos[1]) / boxSize);
        user.direction = direction.left;
        if ((Number.isInteger((user.pos[1]) / boxSize) && y >= 0 && !map[x][y]) || (y >= 0 && !map[x][y] && !map[x + 1][y])) {
            userMoveTo(0, -TANK_SPEED)
        } else {
            userMoveTo(0, 0)
        }
    }
    if (cUp) {
        y = Math.floor((user.pos[0]) / boxSize);
        x = Math.floor((user.pos[1] - TANK_SPEED) / boxSize);
        user.direction = direction.up;
        if ((Number.isInteger((user.pos[0]) / boxSize) && x >= 0 && !map[x][y]) || (x >= 0 && !map[x][y] && !map[x][y + 1])) {
            userMoveTo(1, -TANK_SPEED)
        } else {
            userMoveTo(0, 0)
        }
    }
    if (cRight) {
        y = Math.floor((user.pos[0] + TANK_SPEED + boxSize - 1) / boxSize);
        x = Math.floor((user.pos[1]) / boxSize);
        user.direction = direction.right;
        if ((Number.isInteger((user.pos[1]) / boxSize) && y < map.length && !map[x][y]) || (y < map.length && x < map.length - 1 && !map[x][y] && !map[x + 1][y])) {
            userMoveTo(0, TANK_SPEED)
        } else {
            userMoveTo(0, 0)
        }
    }
    if (cDown) {
        y = Math.floor((user.pos[0]) / boxSize);
        x = Math.floor((user.pos[1] + TANK_SPEED + boxSize - 1) / boxSize);
        user.direction = direction.down;
        if ((Number.isInteger((user.pos[0]) / boxSize) && x < map.length && !map[x][y]) || (x < map.length && y < map[x].length - 1 && !map[x][y] && !map[x][y + 1])) {
            userMoveTo(1, TANK_SPEED)
        } else {
            userMoveTo(0, 0)
        }
    }
    if (shot) {
        if (userBullets.length < MAX_USER_BULLETS && !isBulletCoolDown) {
            const coordinate = {};

            switch (user.direction) {
                case direction.right: {
                    coordinate.x = user.pos[0] + boxSize - bulletSize;
                    coordinate.y = user.pos[1] + boxSize / 2;
                    break;
                }
                case direction.left: {
                    coordinate.x = user.pos[0];
                    coordinate.y = user.pos[1] + boxSize / 2;
                    break;
                }
                case direction.down: {
                    coordinate.x = user.pos[0] + boxSize / 2;
                    coordinate.y = user.pos[1] + boxSize - bulletSize;
                    break;
                }
                case direction.up: {
                    coordinate.x = user.pos[0] + boxSize / 2;
                    coordinate.y = user.pos[1];
                    break;
                }
            }

            userBullets.push(new Bullet({...coordinate, direction: user.direction}));

            isBulletCoolDown = true;

            setTimeout(() => {
                isBulletCoolDown = false;
            }, bulletCoolDown)
        }
    }
}

function bulletsMove() {

    if (!userBullets.length) {
        return;
    }

    for (let i = 0; i < userBullets.length; i++) {

        if (!checkBulletMove(userBullets[i])) {
            delete (userBullets[i]);
        }
    }

    userBullets = userBullets.filter(item => !!item);
}

function checkBulletMove(bullet) {

    if (!bullet) {
        return false;
    }

    if (!bullet.isFirstTick) {
        ctx.fillStyle = '#000';
        ctx.fillRect(bullet.x, bullet.y, Math.round(bulletSize), Math.round(bulletSize));
    } else {
        bullet.isFirstTick = false
    }

    let nextPositionX, nextPositionY;

    switch (bullet.direction) {
        case direction.right: {
            nextPositionX = bullet.x + BULLET_SPEED;

            if (nextPositionX + bulletSize >= map.length * boxSize ||
                map[Math.floor((bullet.y + bulletSize) / boxSize)][Math.floor((nextPositionX + bulletSize) / boxSize)] !== 0 ||
                map[Math.floor((bullet.y) / boxSize)][Math.floor((nextPositionX + bulletSize) / boxSize)] !== 0
            ) {
                return false;
            }
            bullet.x = nextPositionX;

            break;
        }
        case direction.left: {
            nextPositionX = bullet.x - BULLET_SPEED;

            if (nextPositionX <= 0 ||
                map[Math.floor((bullet.y + bulletSize) / boxSize)][Math.floor((nextPositionX) / boxSize)] !== 0 ||
                map[Math.floor((bullet.y) / boxSize)][Math.floor((nextPositionX) / boxSize)] !== 0
            ) {
                return false;
            }
            bullet.x = nextPositionX;

            break;
        }
        case direction.down: {
            nextPositionY = bullet.y + BULLET_SPEED;

            if (nextPositionY + bulletSize >= map.length * boxSize ||
                map[Math.floor((nextPositionY + bulletSize) / boxSize)][Math.floor((bullet.x) / boxSize)] !== 0 ||
                map[Math.floor((nextPositionY + bulletSize) / boxSize)][Math.floor((bullet.x + bulletSize) / boxSize)] !== 0
            ) {
                return false;
            }
            bullet.y = nextPositionY;

            break;
        }
        case direction.up: {
            nextPositionY = bullet.y - BULLET_SPEED;

            if (nextPositionY <= 0 ||
                map[Math.floor((nextPositionY) / boxSize)][Math.floor((bullet.x) / boxSize)] !== 0 ||
                map[Math.floor((nextPositionY) / boxSize)][Math.floor((bullet.x + bulletSize) / boxSize)] !== 0
            ) {
                return false;
            }

            bullet.y = nextPositionY;

            break;
        }
    }

    ctx.fillStyle = 'red';
    ctx.fillRect(bullet.x, bullet.y, Math.round(bulletSize), Math.round(bulletSize));
    return true;
}

// keys
document.onkeydown = function (e) {
    e = e || window.event;
    switch (e.keyCode) {
        case 38: { //up
            cUp = 1;
            break;
        }
        case 40: { //down
            cDown = 1;
            break;
        }
        case 37: { //left
            cLeft = 1;
            break;
        }

        case 39: { //right
            cRight = 1;
            break;
        }

        case 32: { //shot
            shot = 1;
            break;
        }
    }
};
document.onkeyup = function (e) {
    e = e || window.event;
    switch (e.keyCode) {
        case 38: { //up
            cUp = 0;
            break;
        }
        case 40: { //bottom
            cDown = 0;
            break;
        }
        case 37: { //left
            cLeft = 0;
            break;
        }

        case 39: { //right
            cRight = 0;
            break;
        }

        case 32: { //shot
            shot = 0;
            break;
        }
    }
};
